function onSay(cid, words, param)
	local player = Player(cid)
	if not player then
		return false
	end
	local playerStrength = math.floor(getPlayerStrength(player))
	if playerStrength < bgSettings.mostDmg.minPlayerStrength then
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You are not strong enough to join the battlegrounds.\nRequired " .. bgSettings.mostDmg.minPlayerStrength .. " strength, your current strength is " .. playerStrength .. ".")
		return false
	end
	if not mostDmgQueue[cid] then
		local modalMessage = "Please select your queue type.\nYou need a party to queue as party.\nYou need to register a team and be in party with your team to queue as team."
		local modalWindow = ModalWindow(BG_QUEUE, 	"Queue for a battleground!", modalMessage)
		modalWindow:addButton(BG_QUEUE_BUTTON_TEAM, "Team")
		modalWindow:addButton(BG_QUEUE_BUTTON_LEAVE, "Leave")
		modalWindow:addButton(BG_QUEUE_BUTTON_PARTY, "Party")
		modalWindow:addButton(BG_QUEUE_BUTTON_SOLO, "Solo")
		modalWindow:setDefaultEnterButton(BG_QUEUE_BUTTON_SOLO)
		modalWindow:setDefaultEscapeButton(BG_QUEUE_BUTTON_LEAVE)
		modalWindow:sendToPlayer(player)
	else
		local modalMessage = "You are already in the queue.\nYou can choose to leave if you wish, or do nothing."
		local modalWindow = ModalWindow(BG_QUEUE, "Queue for a battleground!", modalMessage)
		modalWindow:addButton(BG_QUEUE_BUTTON_LEAVE, "Leave")
		modalWindow:addButton(BG_QUEUE_BUTTON_NOTHING, "Nothing")
		modalWindow:setDefaultEnterButton(BG_QUEUE_BUTTON_NOTHING)
		modalWindow:setDefaultEscapeButton(BG_QUEUE_BUTTON_NOTHING)
		modalWindow:sendToPlayer(player)
	end
	return false
end