function onSay(cid, words, param)
	local player = Player(cid)
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Auto reply: \"" .. param .. "\"")
	player:autoReply(param)
	return false
end