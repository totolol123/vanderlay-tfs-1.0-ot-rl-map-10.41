function onSay(cid, words, param)
	local player = Player(cid)
	if not player then
		return false
	end
	local totalBlessPrice = getBlessingsCost(player:getLevel()) * 5 * 1.1
	if player:getBlessings() == 5 then
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You already have all blessings.")
	elseif player:removeMoney(totalBlessPrice) then
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You have been blessed by all of five gods, " .. player:getName() .. "!.")
		for b = 1, 5 do
			player:addBlessing(b)
		end
		player:getPosition():sendMagicEffect(CONST_ME_HOLYAREA)
	else
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You dont have enough money. You need " .. totalBlessPrice .. " gold.")
	end
	return false
end