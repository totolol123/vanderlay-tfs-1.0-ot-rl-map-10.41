function onSay(cid, words, param)
	local player 	= Player(cid)
	local cdr 		= player:getSkillLevel(SKILL_COOLDOWN_REDUCTION)
	local atkspeed 	= player:getSkillLevel(SKILL_ATK_SPEED)
	local movespeed = player:getSkillLevel(SKILL_MOVE_SPEED)
	local regen 	= player:getSkillLevel(SKILL_REGENERATION_SPEED)
	local pvpPower 	= player:getSkillLevel(SKILL_PVP_POWER)
	local message 	= "Current stats:"
	
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, message)
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "PvP Power: +" .. pvpPower .. "%")
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Attack speed: +" .. atkspeed .. "%")
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Movement speed: +" .. movespeed .. "%")
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Cooldown Reduction: +" .. cdr .. "%")
	player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Regeneration speed: +" .. regen .. "%")

	return false
end