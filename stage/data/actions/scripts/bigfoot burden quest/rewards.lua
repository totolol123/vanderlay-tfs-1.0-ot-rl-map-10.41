local reward = {
	[3148] = {
		[1] = {itemid = 18501, count = 1, alwaysLoot = 0},
		[2] = {itemid = 18502, count = 1, alwaysLoot = 0},
		[3] = {itemid = 18503, count = 1, alwaysLoot = 0},
		[4] = {itemid = 18402, count = 1, alwaysLoot = 0},
		[5] = {itemid = 18396, count = 1, alwaysLoot = 1},
		[6] = {itemid = 2160,  count = 3, alwaysLoot = 1},
		[7] = {itemid = 18415, count = 7, alwaysLoot = 1},
		[8] = {itemid = 18423, count = 2, alwaysLoot = 1}
	},
	[3149] = {
		[1] = {itemid = 18407, count = 1, alwaysLoot = 0},
		[2] = {itemid = 18505, count = 1, alwaysLoot = 0},
		[3] = {itemid = 18506, count = 1, alwaysLoot = 0},
		[4] = {itemid = 18507, count = 1, alwaysLoot = 0},
		[5] = {itemid = 18396, count = 1, alwaysLoot = 1},
		[6] = {itemid = 2160,  count = 4, alwaysLoot = 1},
		[7] = {itemid = 18413, count = 0, alwaysLoot = 1},
		[8] = {itemid = 18423, count = 2, alwaysLoot = 1}
	},
	[3150] = {
		[1] = {itemid = 18499, count = 1,  alwaysLoot = 0},
		[2] = {itemid = 18498, count = 1,  alwaysLoot = 0},
		[3] = {itemid = 18497, count = 1,  alwaysLoot = 0},
		[4] = {itemid = 18408, count = 1,  alwaysLoot = 0},
		[5] = {itemid = 18396, count = 1,  alwaysLoot = 1},
		[6] = {itemid = 2160,  count = 5,  alwaysLoot = 1},
		[7] = {itemid = 18414, count = 2,  alwaysLoot = 1},
		[8] = {itemid = 18423, count = 12, alwaysLoot = 1}
	},
}

function onUse(cid, item, fromPosition, itemEx, toPosition)
	local player = Player(cid)
	tmpTable = {}
	if item.uid == 3147 then
		if player:getStorageValue(Storage.BigfootBurden.WarzoneStatus) == 4 then
			player:setStorageValue(Storage.BigfootBurden.WarzoneStatus, 5)
			player:addItem(2137, 1)
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You have found some golden fruits.")
		else
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "The chest is empty.")
		end
	elseif item.uid == 3148 then
		if player:getStorageValue(Storage.BigfootBurden.Warzone1Reward) == 1 then
			player:setStorageValue(Storage.BigfootBurden.Warzone1Reward, 0)
			local loot = {}
			local randomLoot = {}
			for i = 1, #reward[item.uid] do
				if reward[item.uid][i].alwaysLoot == 1 then
					loot[reward[item.uid][i].itemid] = reward[item.uid][i].count
				elseif reward[item.uid][i].alwaysLoot == 0 then
					table.insert(randomLoot, reward[item.uid][i])
				end
			end
			if randomLoot then
				local random = math.random(#randomLoot)
				loot[randomLoot[random].itemid] = randomLoot[random].count
			end
			if loot then
				for itemid, count in pairs(loot) do
					player:addItem(itemid, count)
				end
			end
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You have found a reward for defeating Warzone 1.")
		else
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "The chest is empty.")
		end
	elseif item.uid == 3149 then
		if player:getStorageValue(Storage.BigfootBurden.Warzone2Reward) == 1 then
			player:setStorageValue(Storage.BigfootBurden.Warzone2Reward, 0)
			local loot = {}
			local randomLoot = {}
			for i = 1, #reward[item.uid] do
				if reward[item.uid][i].alwaysLoot == 1 then
					loot[reward[item.uid][i].itemid] = reward[item.uid][i].count
				elseif reward[item.uid][i].alwaysLoot == 0 then
					table.insert(randomLoot, reward[item.uid][i])
				end
			end
			if randomLoot then
				local random = math.random(#randomLoot)
				loot[randomLoot[random].itemid] = randomLoot[random].count
			end
			if loot then
				for itemid, count in pairs(loot) do
					player:addItem(itemid, count)
				end
			end
			if math.random(25) == 1 then
				player:addItem(16619, 1)
			end
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You have found a reward for defeating Warzone 2.")
		else
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "The chest is empty.")
		end
	elseif item.uid == 3150 then
		if player:getStorageValue(Storage.BigfootBurden.Warzone3Reward) == 1 then
			player:setStorageValue(Storage.BigfootBurden.Warzone3Reward, 0)
			local loot = {}
			local randomLoot = {}
			for i = 1, #reward[item.uid] do
				if reward[item.uid][i].alwaysLoot == 1 then
					loot[reward[item.uid][i].itemid] = reward[item.uid][i].count
				elseif reward[item.uid][i].alwaysLoot == 0 then
					table.insert(randomLoot, reward[item.uid][i])
				end
			end
			if randomLoot then
				local random = math.random(#randomLoot)
				loot[randomLoot[random].itemid] = randomLoot[random].count
			end
			if loot then
				for itemid, count in pairs(loot) do
					player:addItem(itemid, count)
				end
			end
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "You have found a reward for defeating Warzone 3.")
		else
			player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "The chest is empty.")
		end
	end
	return true
end
