function onThink(interval, lastExecution, thinkInterval)
	local totalPlayersInQueue = countArrayByStringIndex(mostDmgQueue)
	local queueSettings = bgSettings.mostDmg
	playersAcceptJoinArena = nil
	playersAcceptJoinArena = {}
	if totalPlayersInQueue >= queueSettings.minPlayers then
		local arena = getAvailableArena()
		if not arena then
			return true
		end
		local totalRequests = 0
		local playersQueue = getPlayersFromQueue(mostDmgQueue, queueSettings.maxPlayers, totalPlayersInQueue)
		if playersQueue and countArrayByStringIndex(playersQueue) >= queueSettings.minPlayers then
			for cid, playerData in pairs(playersQueue) do
				if totalRequests >= queueSettings.maxPlayers then
					break
				end
				local player = Player(cid)
				if player then
					if playerIsReadyForArena(player, playerData.queueType) then
						local modalMessage = "Do you want to join the battleground?\nIf you do not enter, you will automatically leave the queue.\nYou automatically leave the queue if you do not join within 15 seconds."
						local modalWindow = ModalWindow(BG_JOIN_POPUP, 	"Battleground is ready!", modalMessage)
						modalWindow:addButton(1, "Join")
						modalWindow:addButton(2, "Leave")
						modalWindow:setDefaultEnterButton(1)
						modalWindow:setDefaultEscapeButton(2)
						modalWindow:sendToPlayer(player)
						totalRequests = totalRequests + 1
					else
						player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Unable to join arena. Reason: " .. playerReadyError)
						mostDmgQueue[cid] = nil
					end
				else
					mostDmgQueue[cid] = nil
				end
			end
			addEvent(checkAcceptedPlayers, 16 * 1000, arena, playersQueue)
		end
	end
	return true
end

function checkAcceptedPlayers(arena, playersSentQueueTo)
	local totalPlayersAcceptJoinArena = countArrayByStringIndex(playersAcceptJoinArena)
	local queueSettings = bgSettings.mostDmg
	if totalPlayersAcceptJoinArena >= queueSettings.minPlayers  and totalPlayersAcceptJoinArena <= queueSettings.maxPlayers then
		for cid, player in pairs(playersAcceptJoinArena) do
			local playerPosition = player:getPosition()
			local party = player:getParty()
			local playerParty = nil
			if party and mostDmgQueue[cid].queueType == 'party' then
				local leader 	= party:getLeader()
				playerParty 	= leader:getId()
			end
			arena.players[cid]	= {
				name 				= player:getName(),
				originalPosition 	= {x = playerPosition.x, y = playerPosition.y, z = playerPosition.z},
				team				= nil,
				totalDamage			= 0,
				originalParty 		= playerParty
			}
		end
		startArena(arena)
	elseif totalPlayersAcceptJoinArena > 0 then
		for cid, player in pairs(playersAcceptJoinArena) do
			local informationMessage = "Not enough players accepted. You have been placed back into the queue."
			local messageType = MESSAGE_STATUS_CONSOLE_BLUE
			if mostDmgQueue[cid] then
				if mostDmgQueue[cid].queueType == 'party' then
					local party = player:getParty()
					if party then
						local missingMembers = {}
						for index, partyPlayer in pairs(party:getMembers()) do
							local partyMemberAccepted = false
							for cid, player in pairs(playersAcceptJoinArena) do
								if cid == partyPlayer:getId() then
									partyMemberAccepted = true
								end
							end
							if not partyMemberAccepted then
								missingMembers[partyPlayer:getId()] = partyPlayer:getName()
							end
						end
						local leaderAccepted = false
						for cid, player in pairs(playersAcceptJoinArena) do
							if cid == party:getLeader():getId() then
								leaderAccepted = true
							end
						end
						if not leaderAccepted then
							missingMembers[party:getLeader():getId()] = party:getLeader():getName()
						end
						if table.maxn(missingMembers) > 0 then
							informationMessage = 'Some of your party members did not join the arena.\nYour party has been removed from the queue.\n'
							for guid, name in pairs(missingMembers) do
								informationMessage = informationMessage .. name .. " did not join the arena.\n"
							end
							mostDmgQueue[cid] = nil
						end
					else
						informationMessage = "You queued as party but you have left your party. Please re-enter the queue by typing !bg."
						mostDmgQueue[cid] = nil
					end
				end
			end
			if playersSentQueueTo[cid] then
				playersSentQueueTo[cid] = nil
			end
			player:sendTextMessage(messageType, informationMessage)
		end
		for cid, data in pairs(playersSentQueueTo) do
			mostDmgQueue[cid] = nil
		end
	else
		-- no one has accepted from the playersSentQueueTo, remove them from queue
		for cid, data in pairs(playersSentQueueTo) do
			mostDmgQueue[cid] = nil
		end
	end
	playersAcceptJoinArena = nil
	return true
end