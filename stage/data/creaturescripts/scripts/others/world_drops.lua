local config = {
	attribute_drops = {
		chance = 10,
		minExp = 3000,
		skillTypes = {
			[6541]	= {
				id = SKILL_ATK_SPEED,
				message = "attack speed"
			},
			[6542] = {
				id = SKILL_MOVE_SPEED,
				message = "movement speed"
			},
			[6543] = {
				id = SKILL_COOLDOWN_REDUCTION,
				message = "cooldown reduction"
			},
			[6544] = {
				id = SKILL_REGENERATION_SPEED,
				message = "regeneration speed"
			},
			[6545] = {
				id = SKILL_PVP_POWER,
				message = "pvp power"
			}
		}
	}
}

function onKill(cid, target)
	local targetMonster = Monster(target)
	if not targetMonster then
		return true
	end
	if not targetMonster:isMonster() then
		return true
	end
	local player = Player(cid)
	local experience = MonsterType(targetMonster:getName()):getExperience() * Game.getExperienceStage(player:getLevel()) * 1.5
	if experience < config.attribute_drops.minExp then
		return true
	end
    local chance = math.random(100)
    local pvpPower 	= player:getSkillLevel(SKILL_PVP_POWER)
    local atkSpeed 	= player:getSkillLevel(SKILL_ATK_SPEED)
    local cdr 		= player:getSkillLevel(SKILL_COOLDOWN_REDUCTION)
    if(config.attribute_drops.chance >= chance) then
    	local startId = 6541
    	local lastId = 6545
    	if(pvpPower >= 40) then
    		lastId = 6544
    	elseif 90 >= math.random(100) then
    		lastId = 6544
    	end
    	local skill = config.attribute_drops.skillTypes[math.random(startId, lastId)]
    	local message = skill.message
    	local skillId = skill.id
		local currentSkill = player:getSkillLevel(skillId)
		local newSkill = player:addAttributeSkill(skillId)
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Your " .. message .. " has increased from " .. currentSkill .. "% to " .. newSkill .. "%.")
		player:sendTextMessage(MESSAGE_STATUS_DEFAULT, 'You found an attribute material!')
	end
    return true
end