function onHealthChange(creature, attacker, primaryDamage, primaryType, weaponType, secondaryDamage, secondaryType, origin)
	if attacker and attacker:isPlayer() then
		local criticalChance = attacker:getCriticalChance()
		if criticalChance and criticalChance > math.random(100) then
			primaryDamage = primaryDamage * 1.5
		end

		if weaponType ~= WEAPON_NONE then
			local meleeTypes = {
				WEAPON_SWORD,
				WEAPON_CLUB,
				WEAPON_AXE 
			}
			local weapon = getWeapon(attacker)
			if weapon then
				local itemExp = weapon:getAttribute(ITEM_ATTRIBUTE_EXPERIENCE)
				local itemLevel = getItemLevel(itemExp)
				if itemLevel >= 10 then
					if weaponType == WEAPON_AMMO or weaponType == WEAPON_DISTANCE then
						--piercing bolts
						local weaponCriticalChance = weapon:getAttribute(ITEM_ATTRIBUTE_CRITICALCHANCE)
						if weaponCriticalChance <= 0 then
							weapon:setAttribute(ITEM_ATTRIBUTE_CRITICALCHANCE, 10)
						end
						local min, max = primaryDamage * 0.6, primaryDamage * 0.6
						piercingBolts:setFormula(COMBAT_FORMULA_DAMAGE, -min, -min, -max, -max)
						piercingBolts:execute(attacker:getId(), Variant(creature))
						primaryDamage = primaryDamage - min
					elseif weaponType == WEAPON_WAND then
						-- gain mana on attack
						attacker:addMana(10 + (itemLevel / 2))
					elseif isInArray(meleeTypes, weaponType) then
						-- gain hp on attack
						local weaponLifesteal = weapon:getAttribute(ITEM_ATTRIBUTE_LIFESTEAL)
						if weaponLifesteal <= 0 then
							weapon:setAttribute(ITEM_ATTRIBUTE_LIFESTEAL, 25)
						end
						local lifesteal = attacker:getLifesteal()
						attacker:addHealth(primaryDamage * (lifesteal / 100))
					end
				end
				if itemLevel >= 20 then
					-- Dual wield code
					if isInArray(meleeTypes, weaponType) then
						local weaponSlotType = ItemType(weapon:getId()):getSlotPosition()
						if weaponSlotType ~= 2096 then -- if not two handed weapon
							if weapon:getAttribute(ITEM_ATTRIBUTE_DUALWIELD) <= 0 then
								weapon:setAttribute(ITEM_ATTRIBUTE_DUALWIELD, 1)
							end
							local leftItem = attacker:getSlotItem(CONST_SLOT_LEFT)
							local rightItem = attacker:getSlotItem(CONST_SLOT_RIGHT)
							if isWeapon(leftItem) and isWeapon(rightItem) then
								local attackValue = getWeaponAttack(rightItem)
								local attackSkill = attacker:getWeaponSkill(rightItem)
								local damage = math.random(0, math.ceil((2 * (attackValue * (attackSkill + 5.8) / 25 + (attacker:getLevel() - 1) / 10)) / attacker:getAttackFactor()))
								addEvent(executeDualWield, 150, attacker:getId(), Variant(creature), damage, damage)
								addEvent(dualWieldLifesteal, 150, attacker, damage)
							end
						end
					end
				end
			end
		end
	end
	return primaryDamage, primaryType, secondaryDamage, secondaryType
end

-- Dual wield offhand attack event
function executeDualWield(attackerId, variant, min, max)
	dualWield:setFormula(COMBAT_FORMULA_DAMAGE, -min, -min, -max, -max)
	dualWield:execute(attackerId, variant)
end

function dualWieldLifesteal(player, damage)
	if player then
		local lifesteal = player:getLifesteal()
		player:addHealth(damage * (lifesteal / 100))
	end
end

-- Get a players weapon
function getWeapon(player)
	local equipments = {
		{item = player:getSlotItem(CONST_SLOT_LEFT)},
		{item = player:getSlotItem(CONST_SLOT_RIGHT)}
	}
	local weapon
	for i = 1, #equipments do
		local equipment = equipments[i]
		local item = equipment.item
		if item and item:isItem() then
			local itemType = ItemType(item:getId())
			if itemType:getAttack() > 0 then
				return item
			elseif itemType:getWeaponType() == WEAPON_WAND then
				return item
			end
		end
	end
	return false
end

function getWeaponAttack(item)
	if item and item:isItem() then
		local itemType = ItemType(item:getId())
		return itemType:getAttack()
	end
	return false
end

function isWeapon(item)
	if item and item:isItem() then
		local itemType = ItemType(item:getId())
		if itemType:getAttack() > 0 then
			return true
		else 
			return false
		end
	end
	return false
end