function onModalWindow(player, modalWindowId, buttonId, choiceId)
	if modalWindowId == BG_QUEUE then
		local cid = player:getId()
		local queueType
		local queue
		if buttonId == BG_QUEUE_BUTTON_SOLO then
			queue = mostDmgQueue
			queueType = 'solo'
		elseif buttonId == BG_QUEUE_BUTTON_PARTY then
			local party = player:getParty()
			if not party then
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You must be in a party to queue as party.")
				return false
			end
			if party:getLeader():getId() ~= player:getId() then
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Only the party leader can queue as party.")
				return false
			end
			if (party:getMemberCount() + 1) > (bgSettings.mostDmg.maxPlayers / 2) then
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You have too many members in your party.\nMaximum allowed is: " .. bgSettings.mostDmg.maxPlayers / 2 .. ", you have " .. party:getMemberCount() + 1 .. " members.")
				return false
			end
			queue = mostDmgQueue
			queueType = 'party'
		elseif buttonId == BG_QUEUE_BUTTON_TEAM then
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Team queue is currently not available.")
			return false
		elseif buttonId == BG_QUEUE_BUTTON_LEAVE then
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You have left the queue.")
			queue = mostDmgQueue
			if queue[cid] then
				if queue[cid].queueType == 'party' then
					local party = player:getParty()
					local leader = party:getLeader()
					for index, partyPlayer in pairs(party:getMembers()) do
						if queue[partyPlayer:getId()] then
							partyPlayer:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, player:getName() .. " has left the party. Your party has been removed from the queue.")
							queue[partyPlayer:getId()] = nil
						end
					end
					if queue[leader:getId()] then
						leader:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, player:getName() .. " has left the party. Your party has been removed from the queue.")
						queue[leader:getId()] = nil
					end
				end
				queue[cid] = nil
			end
			return false
		elseif buttonId == BG_QUEUE_BUTTON_NOTHING then
			return false
		end
		if queueType and player:isInsideArena() then 
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You can not queue to an arena while inside an arena.")
			return false
		end
		if queue and (not queue[cid] or queueType == 'party') then
			if not queue[cid] then
				queue[cid] = {
					name 		= player:getName(),
					queueType 	= queueType,
					time		= os.time()
				}
			end
			if queueType == 'party' then
				for index, player in pairs(player:getParty():getMembers()) do
					local pCid = player:getId()
					queue[pCid] 	= {
						name 		= player:getName(),
						queueType 	= 'party',
						time		= os.time()
					}
					player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Your party leader has queued you up for arena.")
				end
			end
		end
		local queueSettings = bgSettings.mostDmg
		local queueMessage = ""
		local totalPlayersInQueue = countArrayByStringIndex(queue)
		local minPlayers = queueSettings.minPlayers-totalPlayersInQueue
		if minPlayers > 0 then
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Total players in queue: " .. totalPlayersInQueue .. ", need " .. minPlayers .. " more players until we can start.")
		else
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Total players in queue: " .. totalPlayersInQueue .. ". Waiting for arena to be available.")
		end
		if totalPlayersInQueue > 0 then
			local i = 1
			for playerId, playerData in pairs(queue) do
				if i == totalPlayersInQueue then
					queueMessage = queueMessage .. playerData.name .. " [" .. Player(playerId):getLevel() .. "]" .. "."
				else
					queueMessage = queueMessage .. playerData.name .. " [" .. Player(playerId):getLevel() .. "]" .. ", "
				end
				i = i + 1
			end
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, queueMessage)
		end
	end
	return true
end