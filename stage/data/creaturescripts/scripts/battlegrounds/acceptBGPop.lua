function onModalWindow(player, modalWindowId, buttonId, choiceId)
	if modalWindowId == BG_JOIN_POPUP then
		local creature = Creature(player)
		local cid = creature:getId()
		if buttonId == 1 then
			if playersAcceptJoinArena and mostDmgQueue[cid] then
				playersAcceptJoinArena[cid] = player
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "Waiting for other players to accept.")
			else
				if mostDmgQueue[cid] then
					mostDmgQueue[cid] = nil
				end
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You took too long to respond. Please re-join the queue by typing !bg solo.")
			end
		else
			if mostDmgQueue[cid] then
				mostDmgQueue[cid] = nil
			end
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You have left the queue.")
		end
	end
	return true
end