function Party:onJoin(player)
	if not player:isInsideArena() then
		for index, partyPlayer in pairs(self:getMembers()) do
			local cid = partyPlayer:getId()
			if mostDmgQueue[cid] then
				if mostDmgQueue[cid].queueType == 'party' then
					player:sendCancelMessage("You can't join the party as the party is in Party Queue for battlegrounds.")
					return false
				end
			end
		end
		local leader = self:getLeader()
		local cid = leader:getId()
		if mostDmgQueue[cid] then
			if mostDmgQueue[cid].queueType == 'party' then
				leader:sendCancelMessage("A player tried to join your party but you are in Party Queue. Please leave the queue before inviting more players.")
				return false
			end
		end
	end
	return true
end

function Party:onLeave(player)
	if not player:isInsideArena() then
		for index, partyPlayer in pairs(player:getParty():getMembers()) do
			local cid = partyPlayer:getId()
			if mostDmgQueue[cid] then
				if mostDmgQueue[cid].queueType == 'party' then
					player:sendCancelMessage("You can't leave the party while in Party Queue.")
					return false
				end
			end
		end
	end
	return true
end

function Party:onDisband()
	return true
end
