local bosses = {
	[3144] = {position = Position(33099, 31950, 10), name = 'deathstrike', restriction = Storage.BigfootBurden.Warzone1Restriction},
	[3145] = {position = Position(33103, 31951, 11), name = 'gnomevil', restriction = Storage.BigfootBurden.Warzone2Restriction},
	[3146] = {position = Position(33081, 31902, 12), name = 'abyssador', checkItemId = 18463, restriction = Storage.BigfootBurden.Warzone3Restriction},
}

function onStepIn(cid, item, position, fromPosition)
	local player = Player(cid)
	if not player then
		return true
	end

	local boss = bosses[item.uid]
	if not boss then
		return false
	end

	local timeLeft = player:getStorageValue(boss.restriction)
	if timeLeft > os.time() then
		player:sendTextMessage(MESSAGE_STATUS_WARNING, "You recently killed " .. boss.name .. " and will not be able to enter the boss room until " .. os.date("%X", timeLeft) .. ".")
		player:teleportTo(fromPosition)
		return true
	end

	if boss.checkItemId then
		if Tile(position):getItemById(boss.checkItemId) then
			return true
		end
	end
	player:teleportTo(boss.position)
	boss.position:sendMagicEffect(CONST_ME_TELEPORT)
	player:sendTextMessage(MESSAGE_EVENT_ADVANCE, 'You have half an hour to heroically defeat ' .. boss.name .. '. Otherwise you\'ll be teleported out by the gnomish emergency device.')
	return true
end
