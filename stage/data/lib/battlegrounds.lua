mostDmgQueue 			= {}
playersInsideArena 		= {}
playerReadyError 		= ''
playerSentQueueEvent	= {}
BG_JOIN_POPUP 			= 1
BG_QUEUE				= 2
BG_QUEUE_BUTTON_SOLO	= 1
BG_QUEUE_BUTTON_PARTY	= 2
BG_QUEUE_BUTTON_TEAM 	= 3
BG_QUEUE_BUTTON_LEAVE	= 4
BG_QUEUE_BUTTON_NOTHING = 5

bgSettings 	= {
	mostDmg 	= {
		minPlayers 					= 2, -- minimum amount of players to start the battle ground
		maxPlayers 					= 20, -- maxmium amount of players in the battleground
		bgTime						= 5, -- how long the battleground should be up for in minutes
		minPlayerStrength			= 180 -- minimum strength to queue for arena
	}
}
mapSettings = {
	maps = {
		id 					= 1,
		available 			= true,
		redPosition 		= {x = 31228, y = 32958, z = 7}, -- north position
		redDoorPosition 	= {x = 31225, y = 32972, z = 7},
		bluePosition		= {x = 31228, y = 33050, z = 7}, -- south position
		blueDoorPosition 	= {x = 31225, y = 33033, z = 7},
		players 			= {},
		leader				= nil, -- we need a leader to announce messages
		mostDamage			= {cid = nil, damage = 0},
		redTeamDamage 		= 0,
		blueTeamDamage 		= 0,
		doorSize			= 8,
		doorId				= 9279,
		doorTransformId  	= 9280
	}
}

function getPlayersFromQueue(queue, amount, totalPlayersInQueue)
	local players = {}
	local tmpQueue = {}
	local currentCount = 0	
	for cid, playerData in pairs(queue) do
		tmpQueue[#tmpQueue + 1] = {
			newData = playerData,
			newCid 	= cid
		}
		currentCount = currentCount + 1
	end
	table.sort(tmpQueue,function(a, b)
							return a.newData.time < b.newData.time
						end)
	local currentCount = 0
	for index, playerData in pairs(tmpQueue) do
		if currentCount < amount then
			if not players[playerData.newCid] then
				if queue[playerData.newCid].queueType == 'party' then
					local player = Player(playerData.newCid)
					local partyCount = player:getParty():getMemberCount() + 1 --getMemberCount does not return leader
					local totalPlayers = currentCount + partyCount
					if totalPlayers <= amount and partyCount <= (totalPlayersInQueue - partyCount) then
						local partyLeaderId = player:getParty():getLeader():getId()
						if not players[partyLeaderId] then
							players[partyLeaderId] = queue[partyLeaderId]
							currentCount = currentCount + 1
						end
						for index, player in pairs(player:getParty():getMembers()) do
							local pCid = player:getId()
							if not players[pCid] then
								players[pCid] = queue[pCid]
								currentCount = currentCount + 1
							end
						end
					end
				else
					players[playerData.newCid] = playerData.newData
					currentCount = currentCount + 1
				end
			end
		else
			break
		end
	end
	return players
end

function getAvailableArena()
	for index, map in pairs(mapSettings) do
		if map.available then
			return map
		end
	end
	return false
end

function playerIsReadyForArena(player, queueType)
	if player:isPzLocked() then
		playerReadyError = "PvP PZ Locked"
		return false
	end

	return true
end

function startArena(arena)
	arena.available = false
	resetGates(arena)
	local teams = balanceTeams(arena.players)
	local blueTeam
	local redTeam
	for teamName, teamProperties in pairs(teams) do
		for cid, name in pairs(teamProperties.players) do
			local player = Player(cid)
			if player then
				if not arena.leader then
					arena.leader = cid
				end
				if not player:isInsideArena() then
					player:switchInsideArena()
				end
				if player:getParty() then
					player:getParty():removeMember(player)
				end
				if teamName == 'blueTeam' then
					if not blueTeam then
						blueTeam = Party(player)
					end
					if blueTeam and not blueTeam:getLeader() then
						blueTeam:setLeader(player)
					end
					if blueTeam then
						blueTeam:addInvite(player)
						blueTeam:addMember(player)
					end
					player:teleportTo(Position(arena.bluePosition.x, arena.bluePosition.y, arena.bluePosition.z))
					player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
					arena.players[cid].team = 'blueTeam'
				elseif teamName == 'redTeam' then
					if not redTeam then
						redTeam = Party(player)
					end
					if redTeam and not redTeam:getLeader() then
						redTeam:setLeader(player)
					end
					if redTeam then
						redTeam:addInvite(player)
						redTeam:addMember(player)
					end
					player:teleportTo(Position(arena.redPosition.x, arena.redPosition.y, arena.redPosition.z))
					player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
					if not player:isInsideArena() then
						player:switchInsideArena()
					end
					arena.players[cid].team = 'redTeam'
				end

				-- Set full hp/mana
				player:addHealth(player:getMaxHealth())
				player:addMana(player:getMaxMana())
				
				-- Add PZ lock to players during battleground
				local pzLocked = Condition(CONDITION_INFIGHT)
				pzLocked:setParameter(CONDITION_PARAM_TICKS, -1)
				player:addCondition(pzLocked)

				playersInsideArena[cid] = arena
			end
			mostDmgQueue[cid] = nil
		end
	end
	startGatesEvent(arena, 20)
	arenaBroadcast(arena.players, 'The battleground starts in 20 seconds!')
	addEvent(stopArena, bgSettings.mostDmg.bgTime * 60 * 1000, arena)
end

function stopArena(arena)
	arenaBroadcast(arena.players, nil, nil, 20)
	addEvent(arenaBroadcast, 18 * 1000, arena.players, 'Teleporting players out of the arena.')
	addEvent(closeArena, 20 * 1000, arena)
end

function calculateArenaDamage(arena)
	for cid, playerProperties in pairs(arena.players) do
		local player = Creature(cid)
		if player then
			for attackerId, damage in pairs(player:getDamageMap()) do
				local attackerPlayer = Player(attackerId)
				if attackerPlayer then
					if damage.total then
						if attackerId ~= cid or arena.players[attackerId].team ~= arena.players[cid].team then
							arena.players[attackerId].totalDamage = arena.players[attackerId].totalDamage + damage.total
							if arena.players[attackerId].totalDamage > arena.mostDamage.damage then
								arena.mostDamage.damage = arena.players[attackerId].totalDamage
								arena.mostDamage.cid 	= attackerId
							end
							if arena.players[attackerId].team == 'redTeam' then
								arena.redTeamDamage = arena.redTeamDamage + arena.players[attackerId].totalDamage
							else
								arena.blueTeamDamage = arena.blueTeamDamage + arena.players[attackerId].totalDamage
							end
						end
					end
				end
			end
		end
	end
end

function calculatePlayerDamage(arena, cid)
	local player = Creature(cid)
	for attackerId, damage in pairs(player:getDamageMap()) do
		local attackerPlayer = Player(attackerId)
		if attackerPlayer then
			arena.players[attackerId].totalDamage = arena.players[attackerId].totalDamage + damage.total
			if arena.players[attackerId].totalDamage > arena.mostDamage.damage then
				arena.mostDamage.damage = arena.players[attackerId].totalDamage
				arena.mostDamage.cid 	= attackerId
			end
		end
	end
end

function closeArena(arena)
	calculateArenaDamage(arena)
	for cid, playerProperties in pairs(arena.players) do
		local player = Player(cid)
		if player then
			if player:isInsideArena() then
				player:switchInsideArena()

				local playerPos = arena.players[cid].originalPosition
				local party = player:getParty()
				if party then
					party:disband()
				end
				-- Teleport player out from the arena to original position
				player:teleportTo(Position(playerPos.x, playerPos.y, playerPos.z))
				player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
				playersInsideArena[cid] = nil
				-- Remove skull and PZ
				local skull = player:getSkull()
				if skull ~= SKULL_RED and skull ~= SKULL_BLACK then
					player:setSkull(SKULL_NONE)
				end
				doRemoveCondition(cid, CONDITION_INFIGHT)

				-- Set full hp/mana
				player:addHealth(player:getMaxHealth())
				player:addMana(player:getMaxMana())

				message = "You dealt " .. playerProperties.totalDamage .. " damage."
				if cid == arena.mostDamage.cid then
					message = "You dealt the most damage! You dealt " .. playerProperties.totalDamage .. " damage."
				end
				local winLoseMessage = '%s Team total damage dealt %d vs enemy team total damage %d.'
				local teamWon = ''
				if arena.redTeamDamage > arena.blueTeamDamage then
					teamWon = 'redTeam'
				else
					teamWon = 'blueTeam'
				end
				if playerProperties.team == teamWon then
					if playerProperties.team == 'redTeam' then
						winLoseMessage = string.format(winLoseMessage, 'Your team won!', arena.redTeamDamage, arena.blueTeamDamage)
						addPlayerExperience(player, 1)
					else
						winLoseMessage = string.format(winLoseMessage, 'Your team won!', arena.blueTeamDamage, arena.redTeamDamage)
						addPlayerExperience(player, 1)
					end
				else
					if playerProperties.team == 'redTeam' then
						winLoseMessage = string.format(winLoseMessage, 'Your team lost!', arena.redTeamDamage, arena.blueTeamDamage)
						addPlayerExperience(player, 0)
					else
						winLoseMessage = string.format(winLoseMessage, 'Your team lost!', arena.blueTeamDamage, arena.redTeamDamage)
						addPlayerExperience(player, 0)
					end
				end
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, winLoseMessage)
				player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, message)
			end
		end
	end
	for cid, playerProperties in pairs(arena.players) do 
		if arena.players[cid].originalParty then
			local leader = Player(arena.players[cid].originalParty)
			local newParty = leader:getParty()
			if not newParty then
				newParty = Party(leader)
				if newParty and not newParty:getLeader() then
					newParty:setLeader(leader)
				end
			end
			if newParty then
				local player = Player(cid)
				if not player:getParty() then
					newParty:addInvite(player)
					newParty:addMember(player)
				end
			end
		end
	end
	arena.blueTeamDamage 	= 0
	arena.redTeamDamage 	= 0
	arena.available 		= true
	arena.players 			= {}
	arena.leader 			= nil
end

function arenaBroadcast(players, message, messageType, countDown)
	if message == nil then
		message = "The battleground will end in %d seconds"
	end
	local newMessage = message
	if countDown then
		newMessage = string.format(message, countDown)
	end
	if messageType == nil then
		messageType = MESSAGE_STATUS_WARNING
	end
	for cid, name in pairs(players) do
		local player = Player(cid)
		player:sendTextMessage(messageType, newMessage)
	end
end

function resetGates(arena)
	local blueDoorPosition = arena.blueDoorPosition
	if Tile(Position(blueDoorPosition)):getItemById(arena.doorId) then
		gateSwitch(arena, 'open')
	end
	return true
end

function getPlayerArena(cid)
	return playersInsideArena[cid]
end

function balanceTeams(players)
	local teams = {
		blueTeam = {
			strength 	= 0,
			players 	= {} -- player cids in this array
		},
		redTeam  = {
			strength 	= 0,
			players 	= {} -- player cids in this array
		}
	}

	local parties = {
		leaders = {}
	}
	for cid, playerProperties in pairs(players) do
		if mostDmgQueue[cid].queueType == 'party' then
			local party = Player(cid):getParty()
			if party then
				local leader = party:getLeader()
				if not parties.leaders[leader:getId()] then
					local teamStrength = getPlayerStrength(leader)
					local partyPlayers = {}
					partyPlayers[leader:getId()] = leader:getName()
					for index, player in pairs(party:getMembers()) do
						partyPlayers[player:getId()] = player:getName()
						local playerStrength = getPlayerStrength(player)
						teamStrength = teamStrength + playerStrength
					end
					parties.leaders[leader:getId()] = {
						players = partyPlayers,
						strength = teamStrength
					}
				end
			end
		end
	end
	if parties.leaders then
		for leaderId, partyProperties in pairs(parties.leaders) do
			if teams.blueTeam.strength > teams.redTeam.strength then
				teams.redTeam.strength = teams.redTeam.strength + partyProperties.strength
				for cid, name in pairs(partyProperties.players) do
					teams.redTeam.players[cid] = name
				end
			else
				teams.blueTeam.strength = teams.blueTeam.strength + partyProperties.strength
				for cid, name in pairs(partyProperties.players) do
					teams.blueTeam.players[cid] = name
				end
			end
		end
	end

	for cid, playerProperties in pairs(players) do
		if not Player(cid):getParty() then
			local strength = getPlayerStrength(Player(cid))
			if teams.blueTeam.strength > teams.redTeam.strength then
				teams.redTeam.strength = teams.redTeam.strength + strength
				teams.redTeam.players[cid] = playerProperties.name
			else
				teams.blueTeam.strength = teams.blueTeam.strength + strength
				teams.blueTeam.players[cid] = playerProperties.name
			end
		end
	end

	return teams
end

function getPlayerStrength(player)
	local strength = 0
	local strengthTypes = {
		[SKILL_FIST] 		= SKILL_ATK_SPEED,
		[SKILL_AXE]			= SKILL_ATK_SPEED,
		[SKILL_DISTANCE]	= SKILL_ATK_SPEED,
		[SKILL_SWORD]		= SKILL_ATK_SPEED,
		[SKILL_CLUB]		= SKILL_ATK_SPEED
	}
	local primarySkill = 0
	for skillType, multiplier in pairs(strengthTypes) do
		local skillStrength = (player:getSkillLevel(skillType) * (1 + (player:getSkillLevel(multiplier) / 100)))
		if skillStrength > primarySkill then
			primarySkill = skillStrength
		end
	end
	primarySkill = primarySkill + (player:getMagicLevel() * (1 + (player:getSkillLevel(SKILL_COOLDOWN_REDUCTION) / 100)))
	strength = strength + primarySkill
	return strength * (1 + (player:getSkillLevel(SKILL_PVP_POWER) / 100)) + player:getLevel()
end

function gateBroadcast(arena, message, messageType, countDown)
	local player = Player(arena.leader)
	local newMessage = message
	if countDown then
		newMessage = string.format(message, countDown)
	end
	if player then
		if messageType == 'gameStart' then
			if countDown > 0 then
				arena.redDoorPosition.x = arena.redDoorPosition.x + 3
				arena.blueDoorPosition.x = arena.blueDoorPosition.x + 3
				player:say(newMessage, TALKTYPE_MONSTER_SAY, false, nil, Position(arena.blueDoorPosition))
				player:say(newMessage, TALKTYPE_MONSTER_SAY, false, nil, Position(arena.redDoorPosition))
				arena.redDoorPosition.x = arena.redDoorPosition.x - 3
				arena.blueDoorPosition.x = arena.blueDoorPosition.x - 3
			end
			if countDown > 0 then
				if countDown <= 5 then
					addEvent(gateBroadcast, 1000, arena, message, messageType, countDown - 1)
				elseif countDown == 10 then
					addEvent(gateBroadcast, 5000, arena, message, messageType, countDown - 5)
				elseif countDown == 15 then
					addEvent(gateBroadcast, 5000, arena, message, messageType, countDown - 5)
				elseif countDown == 20 then
					addEvent(gateBroadcast, 5000, arena, message, messageType, countDown - 5)
				end
			else
				startGatesEvent(arena)
			end
		end
	end
end

function startGatesEvent(arena, countDown)
	local message = nil
	if countDown and countDown >= 20 then
		message = 'Game starts in %d seconds.'
	end
	local blueDoorPosition = arena.blueDoorPosition
	if Tile(Position(blueDoorPosition)):getItemById(arena.doorId) then
		gateSwitch(arena, 'open')
		if not message then
			message = 'Gates closes in %d seconds.'
		end
		if not countDown then
			countDown = 5
		end
		addEvent(gateBroadcast, 1000, arena, message, 'gameStart', countDown)
	else
		gateSwitch(arena, 'close')
		if not message then
			message = 'Gates opens in %d seconds.'
		end
		if not countDown then
			countDown = 15
		end
		addEvent(gateBroadcast, 1000, arena, message, 'gameStart', countDown)
	end
end

function gateSwitch(arena, switchType)
	if switchType == 'open' then
		local redDoorPosition, blueDoorPosition = arena.redDoorPosition, arena.blueDoorPosition
		for i = 1, arena.doorSize, 1 do
			if Tile(Position(redDoorPosition)):getItemById(arena.doorId) then
				Tile(Position(redDoorPosition)):getItemById(arena.doorId):transform(arena.doorTransformId)
			end
			redDoorPosition.x = redDoorPosition.x + 1

			if Tile(Position(blueDoorPosition)):getItemById(arena.doorId) then
				Tile(Position(blueDoorPosition)):getItemById(arena.doorId):transform(arena.doorTransformId)
			end
			blueDoorPosition.x = blueDoorPosition.x + 1
		end
		blueDoorPosition.x 	= blueDoorPosition.x - arena.doorSize
		redDoorPosition.x 	= redDoorPosition.x - arena.doorSize
	elseif switchType == 'close' then
		local redDoorPosition, blueDoorPosition = arena.redDoorPosition, arena.blueDoorPosition
		for i = 1, arena.doorSize, 1 do
			if Tile(Position(redDoorPosition)):getItemById(arena.doorTransformId) then
				if Tile(Position(redDoorPosition)):getCreatures() then
					for index, userData in pairs(Tile(Position(redDoorPosition)):getCreatures()) do
						local playerPos = userData:getPosition()
						userData:teleportTo(Position(playerPos.x, playerPos.y - 1, playerPos.z), true)
					end
				end
				Tile(Position(redDoorPosition)):getItemById(arena.doorTransformId):transform(arena.doorId)
			end
			redDoorPosition.x = redDoorPosition.x + 1

			if Tile(Position(blueDoorPosition)):getItemById(arena.doorTransformId) then
				if Tile(Position(blueDoorPosition)):getCreatures() then
					for index, userData in pairs(Tile(Position(blueDoorPosition)):getCreatures()) do
						local playerPos = userData:getPosition()
						userData:teleportTo(Position(playerPos.x, playerPos.y + 1, playerPos.z), true)
					end
				end
				Tile(Position(blueDoorPosition)):getItemById(arena.doorTransformId):transform(arena.doorId)
			end
			blueDoorPosition.x = blueDoorPosition.x + 1
		end
		redDoorPosition.x 	= redDoorPosition.x - arena.doorSize
		blueDoorPosition.x 	= blueDoorPosition.x - arena.doorSize
	end
	return true
end

-- player 	Player
-- winLoss 	int -- 1 == win, 0 == loss

function addPlayerExperience(player, winLoss)
	if player then
		local currentExpLevel = getExpRequiredForLevel(player:getLevel())
		local expNeeded = getExpRequiredForLevel(player:getLevel() + 1) - currentExpLevel
		local bgExp = math.floor(expForBG(player:getLevel(), expNeeded) * 0.35)
		if winLoss == 0 then
			bgExp = math.floor(bgExp * 0.70);
		end
		player:addExperience(bgExp, false)
		player:sendTextMessage(MESSAGE_STATUS_DEFAULT, "You gained " .. bgExp .. " experience points for fighting inside the battleground.")
	end
end

function expForBG(level, expNeeded)
	local multiplier = Game.getExperienceStage(level)
	if multiplier then
		multiplier = multiplier / 10
	else
		multiplier = 0.03
	end
	return math.floor(math.pow(math.log10(expNeeded * multiplier), 8))
end

function getExpRequiredForLevel(level)
	level = level - 1
	return ((50 * level * level * level) - (150 * level * level) + (400 * level)) / 3
end