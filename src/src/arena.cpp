/**
* The Forgotten Server - a free and open-source MMORPG server emulator
* Copyright (C) 2014  Mark Samman <mark.samman@gmail.com>
*
* This addition was created by Vanderlay.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "otpch.h";
#include "arena.h"

Arena::Arena()
{
	arena_rating = 0;
	world_rating = 0;
	points = 0;
	won = 0;
	lost = 0;
	tie = 0;
}

Arena::~Arena()
{
	// destructor
}

uint16_t Arena::getWinLoseRating(Arena* selfStats, Arena* enemyStats, bool win)
{
	uint8_t baseWinRating = 24;
	uint8_t baseLoseRating = 24;
	if (getTotalGames(selfStats) < 30 && selfStats->arena_rating < 2300) {
		baseWinRating = 49;
		baseLoseRating = 6;
	} else if (selfStats->arena_rating < 2400) {
		baseWinRating = 32;
		baseLoseRating = 32;
	}
	uint8_t rating;
	if (win) {
		double selfExpo = (enemyStats->arena_rating - selfStats->arena_rating) / 400.;
		double selfWinChance = 1 / (1 + pow(10, selfExpo));
		rating = round(selfStats->arena_rating + baseWinRating * (1 - selfWinChance));
	} else {
		double enemyExpo = (selfStats->arena_rating - enemyStats->arena_rating) / 400.;
		double enemyWinChance = 1 / (1 + pow(10, enemyExpo));
		rating = round(enemyStats->arena_rating + baseLoseRating * (0 - enemyWinChance));
	}

	return rating;
}

uint32_t Arena::getTotalGames(Arena* stats)
{
	return stats->won + stats->lost + stats->tie;
}

uint16_t Arena::updatePoints(uint16_t rating, uint8_t mutliplier)
{
	uint16_t points = rating / (1 + 1639.28 * pow(2.71828, (-0.00412 * rating)));
	return floor(points * (mutliplier / 100.));
}