/**
* The Forgotten Server - a free and open-source MMORPG server emulator
* Copyright (C) 2014  Mark Samman <mark.samman@gmail.com>
* 
* This addition was created by Vanderlay.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

class Arena
{
	friend class IOLoginData;
	public:
		Arena();
		~Arena();
		uint16_t getWinLoseRating(Arena* selfStats, Arena* enemyStats, bool win);
		uint32_t getTotalGames(Arena* stats);
		uint16_t Arena::updatePoints(uint16_t rating, uint8_t multiplier);
	
	protected:
		uint16_t arena_rating;
		uint16_t world_rating;
		uint16_t points;
		uint16_t won;
		uint16_t lost;
		uint16_t tie;
};